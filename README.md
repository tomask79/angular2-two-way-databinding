# Writing applications in Angular 2, [part 6] #

Well, this is going to be my last post regarding component interactions in [Angular 2](https://angular.io/). In this repo let's take a look on the **two-way databinding**
performed by Angular 2. 

## Two way databinding in the Angular 2 ##

### Principle ###

Two way databinding is a mechanism where data flows from parent to child and vice versa. In the other words, when data flows from the model to the view and back. 
Angular 2 doesn't have generic mechanism for the two way databinding, it uses [React JS](https://facebook.github.io/react/docs/two-way-binding-helpers.html) 
principle with unidirectional flowing of data. What does that mean? In Angular 2, **if you want to expose from your component **two-way databinding interface**  
(*input to your component can be changed inside of your component and returned back to input model*) you need to understand the following difference:

**Angular 2, one-way binding:**

Here data flows from parent to child and NOT BACK! Code schema:

Parent:

```
<child-component [child-field-property]="expression"></child-component>
```

Child:

```
@Component({
    selector: 'child-component',
    .
    .
})
export class ChildComponent {
    @Input() child-field-property: ....input type...; 

```

Here we're binding parent value set by the **expression** into the child component's **child-field-property**, one way. **Any changes of the input inside of the 
child-component won't be propagated back to parent**.

** Angular 2, two-way databinding **

Now let's get to the point! If you want to propagate your changes inside of the child component back to parent, 
then you need to define it in following schema:

Goal: We want to inject the input data into childField of the child-component via the expression pointing to data in the parent
and propagate back any possible changes in the child component.

Parent: 
```
<child-component [(childField)]="expression"></child-component>
```

Child:
```
@Component({
    selector: 'child-component',
    .
    .
})

export class ChildComponent {
    @Input()  childField: ...child type...;
    @Output() childFieldChange = new EventEmitter<child type>();     

    someChildEvent(...params...) : void {
        ....change childField....;
        // emit child data back to parent...
        this.childFieldChange.emit(this.childField);
    }
}
```
**[(childField)]="expression"** is actually shorter form of:

```
<child-component [childField]="expression" (childFieldChange)="expression = $event"></child-component>
```

And that's the whole point, two-way databinding in Angular 2 is actually combination of two unidirectional data flow set by the Input decorator
and EventEmitter.

## Working demo ##

Let's change my example with product list and basket to be using two-way databinding. First, let's remove the basket component. And for example,
let's create component which is going to be showing list of bought products:


```
import {Component, Input} from 'angular2/core';
import {ProductType} from './product';

@Component({
    selector: 'bought-products',
    template: `
               <hr>
               <h3>Selected products into basket</h3>
               <ul>
                    <li *ngFor="#prod of boughtProductList">
                        {{prod.name}}
                    </li>
                    <button (click) = "clearBoughtProducts()">CLEAR</button>
               </ul>
              `
})
export class BoughtProductsComponent {
    @Input() boughtProductList: ProductType[]; 

    clearBoughtProducts(): void {
        // this._boughtProducts = [];
        this.boughtProductList.length = 0;
    }
}
```
All we need is to put the Input into "boughtProductList" property. **Let's change product-detail component to be exposing two-databinding interface for bought products **:

```
import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {ProductType} from './product';

@Component({
    selector: 'product-detail',
    template: `
        <div>{{product.name}} for {{product.price}}$ <button (click)="fireProductBuyEvent(product)">BUY</button></div>
        <img src={{product.image}} />
    `    
})

export class ProductDetailComponent {
    @Input() product: ProductType;
    @Input() ngBoughtProduct: ProductType[]; 
    @Output() ngBoughtProductChange = new EventEmitter<ProductType[]>();     

    fireProductBuyEvent(product: ProductType) : void {
        this.ngBoughtProduct.push(product);
        this.ngBoughtProductChange.emit(this.ngBoughtProduct);
    }
}
```
Now, any bought product will be emitted up through the two-way databinding via ngBoughtProduct field up to parent's input field pointed to ngBoughtProduct.
**!!!Important!!! To have the two-way databinding working, EventEmitter field needs to have the name: twoWayDatabindedField plus suffix Change**. For example here:
We've got two way databinded field **ngBoughtProduct**, so to have the two way databinding working, we need to create **ngBoughtProductChange** emitter...

Now parent component passing the two way databinded selected products:

```
import {Component, OnInit, ViewChild} from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {ProductDetailComponent} from './product.detail.component';
import {BoughtProductsComponent} from './bought-products.component';

@Component({
    selector: 'my-app',
    template: `<h1>Two way databinding demo</h1>
            <div>
                <bought-products [boughtProductList] = "_boughtProducts"></bought-products>
            </div>
            <hr>
            <ul>
                <li *ngFor="#product of _products">
                    <product-detail [(ngBoughtProduct)] = "_boughtProducts" 
                                    [product]="product">
                    </product-detail>
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [ProductDetailComponent, BoughtProductsComponent]
})
export class AppComponent implements OnInit {
     private _products: ProductType[];
     private _boughtProducts: ProductType[] = [];

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }
}
```
* Notice **[(ngBoughtProduct)] = "_boughtProducts"**, I'm saying here: Hey product detail, please save any selected product into _boughtProducts array field".
* <bought-products [boughtProductList] = "_boughtProducts"></bought-products> And here: "Hey Angular 2, display my two-way databinded list of bought products(in the _boughtProducts field) via the bought-products component".

Isn't Angular 2 nice?...:)

## Running the demo ##

* git clone <this repo>
* npm install
* npm start
* visit localhost:3000

Hope you found this useful!

regards

Tomas