import {Component, OnInit, ViewChild} from 'angular2/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {ProductDetailComponent} from './product.detail.component';
import {BoughtProductsComponent} from './bought-products.component';

@Component({
    selector: 'my-app',
    template: `<h1>Two way databinding demo</h1>
            <div>
                <bought-products [boughtProductList] = "_boughtProducts"></bought-products>
            </div>
            <hr>
            <ul>
                <li *ngFor="#product of _products">
                    <product-detail [(ngBoughtProduct)] = "_boughtProducts" 
                                    [product]="product">
                    </product-detail>
                </li>
            </ul>           
    `,
    providers: [ProductService],
    directives: [ProductDetailComponent, BoughtProductsComponent]
})
export class AppComponent implements OnInit {
     private _products: ProductType[];
     private _boughtProducts: ProductType[] = [];

    constructor(private productService: ProductService) {
    }

    ngOnInit() : void {
        this._products = this.productService.getProducts();
    }
}