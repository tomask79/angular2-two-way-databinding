System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var BoughtProductsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            BoughtProductsComponent = (function () {
                function BoughtProductsComponent() {
                }
                BoughtProductsComponent.prototype.clearBoughtProducts = function () {
                    // this._boughtProducts = [];
                    this.boughtProductList.length = 0;
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], BoughtProductsComponent.prototype, "boughtProductList", void 0);
                BoughtProductsComponent = __decorate([
                    core_1.Component({
                        selector: 'bought-products',
                        template: "\n               <hr>\n               <h3>Selected products into basket</h3>\n               <ul>\n                    <li *ngFor=\"#prod of boughtProductList\">\n                        {{prod.name}}\n                    </li>\n                    <button (click) = \"clearBoughtProducts()\">CLEAR</button>\n               </ul>\n              "
                    }), 
                    __metadata('design:paramtypes', [])
                ], BoughtProductsComponent);
                return BoughtProductsComponent;
            }());
            exports_1("BoughtProductsComponent", BoughtProductsComponent);
        }
    }
});
//# sourceMappingURL=bought-products.component.js.map