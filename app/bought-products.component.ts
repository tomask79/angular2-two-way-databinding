import {Component, Input} from 'angular2/core';
import {ProductType} from './product';

@Component({
    selector: 'bought-products',
    template: `
               <hr>
               <h3>Selected products into basket</h3>
               <ul>
                    <li *ngFor="#prod of boughtProductList">
                        {{prod.name}}
                    </li>
                    <button (click) = "clearBoughtProducts()">CLEAR</button>
               </ul>
              `
})
export class BoughtProductsComponent {
    @Input() boughtProductList: ProductType[]; 

    clearBoughtProducts(): void {
        // this._boughtProducts = [];
        this.boughtProductList.length = 0;
    }
}