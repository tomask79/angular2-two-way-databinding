System.register(['angular2/core', './product'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, product_1;
    var ProductDetailComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (product_1_1) {
                product_1 = product_1_1;
            }],
        execute: function() {
            ProductDetailComponent = (function () {
                function ProductDetailComponent() {
                    this.ngBoughtProductChange = new core_1.EventEmitter();
                }
                ProductDetailComponent.prototype.fireProductBuyEvent = function (product) {
                    this.ngBoughtProduct.push(product);
                    this.ngBoughtProductChange.emit(this.ngBoughtProduct);
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', product_1.ProductType)
                ], ProductDetailComponent.prototype, "product", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], ProductDetailComponent.prototype, "ngBoughtProduct", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], ProductDetailComponent.prototype, "ngBoughtProductChange", void 0);
                ProductDetailComponent = __decorate([
                    core_1.Component({
                        selector: 'product-detail',
                        template: "\n        <div>{{product.name}} for {{product.price}}$ <button (click)=\"fireProductBuyEvent(product)\">BUY</button></div>\n        <img src={{product.image}} />\n    "
                    }), 
                    __metadata('design:paramtypes', [])
                ], ProductDetailComponent);
                return ProductDetailComponent;
            }());
            exports_1("ProductDetailComponent", ProductDetailComponent);
        }
    }
});
//# sourceMappingURL=product.detail.component.js.map