import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {ProductType} from './product';

@Component({
    selector: 'product-detail',
    template: `
        <div>{{product.name}} for {{product.price}}$ <button (click)="fireProductBuyEvent(product)">BUY</button></div>
        <img src={{product.image}} />
    `    
})

export class ProductDetailComponent {
    @Input() product: ProductType;
    @Input() ngBoughtProduct: ProductType[]; 
    @Output() ngBoughtProductChange = new EventEmitter<ProductType[]>();     

    fireProductBuyEvent(product: ProductType) : void {
        this.ngBoughtProduct.push(product);
        this.ngBoughtProductChange.emit(this.ngBoughtProduct);
    }
}